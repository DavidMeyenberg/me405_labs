'''
@file TouchPanel.py

@brief This code creates a TouchPanelDriver class to read touch inputs on the touch panel.

@author David Meyenberg

@date March 4, 2021
'''

import utime

class TouchPanelDriver:
	'''
	@brief Allows the user to communicate with a touch panel.
	'''
	
	def __init__(self, xp, xm, yp, ym, width, length):    
		'''
		@brief Creates a TouchPanelDriver object that can scan for touch input.
		'''
        # pin that connects to positve X
		self.Xp = xp
		
		# pin that connects to negative X
		self.Xm = xm
		
		# pin that connects to positve Y
		self.Yp = yp
		
		# pin that connects to negative Y
		self.Ym = ym

		# width of touch panel
		self.Width = width

		# length of touch panel
		self.Length = length
		

	def ScanX(self):
		'''
		@brief Scans the X axis for input on the touch pad		   
		'''
		ADC_XMAX = 3800 # maximum ADC value measured when touching touch pad
		ADC_XMIN = 195 # minimum ADC value measured when touching touch pad
		X_CENTER = 102 # calibrated center of touch pad in mm
		X_p = pyb.Pin(self.Xp, mode = pyb.Pin.OUT_PP) # sets positive X to push pull
		X_m = pyb.Pin(self.Xm, mode = pyb.Pin.OUT_PP) # sets negative X to push pull
		Y_m = pyb.ADC(self.Ym)  #creates ADC object for negative Y
		Y_p = pyb.Pin(self.Yp, mode = pyb.Pin.IN)  #sets positive Y as input to make it float
		X_p.high() #set positive X high
		X_m.low()  #set negative X low
		utime.sleep_us(5)  #delay to allow time for components to settle
		return Y_m.read() * self.Length/(ADC_XMAX - ADC_XMIN) - X_CENTER  #read from ADC object at negative Y
		 
	
	def ScanY(self):
		'''
		@brief Scans the Y axis for input on the touch pad		   
		'''
		ADC_YMAX = 3590 # maximum ADC value measured when touching touch pad
		ADC_YMIN = 375 # minimum ADC value measured when touching touch pad
		Y_CENTER = 63 # calibrated center of touch pad in mm
		Y_p = pyb.Pin(self.Yp, mode = pyb.Pin.OUT_PP) # sets positive Y to push pull
		Y_m = pyb.Pin(self.Ym, mode = pyb.Pin.OUT_PP) # sets negative Y to push pull
		X_m = pyb.ADC(self.Xm)  #create ADC object for negative X
		X_p = pyb.Pin(self.Xp, mode = pyb.Pin.IN)  #sets positive X as input to make it float
		Y_p.high() #set positive Y high
		Y_m.low()  #set negative Y low
		utime.sleep_us(5)  #delay to allow time for components to settle
		return X_m.read() * self.Width/(ADC_YMAX-ADC_YMIN) - Y_CENTER  #read from ADC object at negative X
		
	def ScanZ(self):
		'''
		@brief Scans the Y axis for input on the touch pad		   
		'''
		Y_p = pyb.Pin(self.Yp, mode = pyb.Pin.OUT_PP) # sets positive Y to push pull
		X_m = pyb.Pin(self.Xm, mode = pyb.Pin.OUT_PP) # sets negative X to push pull
		Y_m = pyb.ADC(self.Ym)  #create ADC object for negative X
		X_p = pyb.Pin(self.Xp, mode = pyb.Pin.IN)  #sets positive X as input to make it float
		Y_p.high() #set positive Y high
		X_m.low()  #set negative Y low
		utime.sleep_us(5)  #delay to allow time for components to settle
		return Y_m.read() < 4000   #read from ADC object at negative X

	def Scan(self):
		'''
		@brief Scans the X, Y, and Z axis for input on the touch pad		   
		'''
		return (self.ScanX(), self.ScanY(), self.ScanZ())
		        
if __name__ == "__main__":
	#width of 105 and length of 182 measured as distances between maximum and minimum readings
	TPD = TouchPanelDriver(pyb.Pin.board.PA7, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA0, 105, 182)
	for(True):
		start_time = utime.ticks_us()
		xScanValue = TPD.ScanX()
		end_time = utime.ticks_us()
		total_time = utime.ticks_diff(end_time, start_time)
		print("X scan took " + str(total_time) + " microseconds")
		print("X value: " + str(xScanValue) + "\n")

		start_time = utime.ticks_us()
		yScanValue = TPD.ScanY()
		end_time = utime.ticks_us()
		total_time = utime.ticks_diff(end_time, start_time)
		print("Y scan took " + str(total_time) + " microseconds")
		print("Y value: " + str(yScanValue) + "\n")

		start_time = utime.ticks_us()
		zScanValue = TPD.ScanZ()
		end_time = utime.ticks_us()
		total_time = utime.ticks_diff(end_time, start_time)
		print("Z scan took " + str(total_time) + " microseconds")
		print("Z value: " + str(zScanValue) + "\n")
		start_time = utime.ticks_us()	
		scan = TPD.Scan()
		end_time = utime.ticks_us()
		total_time = utime.ticks_diff(end_time, start_time)
		print("Scan took " + str(total_time) + " microseconds")
		print("Scan Results: " + str(scan[0]) + ", " + str(scan[1]) + ", " + str(scan[2]) + "\n")
		utime.sleep(1)