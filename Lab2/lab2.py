## @file lab2.py
#  This program tests a user's reaction time by flashing an LED and recording the time it takes to press the button
#
#  The program displays a message letting the user know the LED will flash in 2-3 seconds. After waiting between 2 and 3 seconds, it turns on the LED.
#  It waits for the user to press the user button (B1), which triggers an interrupt that records the timer's current value.  This time in seconds gets displayed to the user. 
#  The program waits a few seconds and then resets with the original message. 
#
#  @author David Meyenberg
#
#  @copyright License Info
#
#  @date January 28, 2021
#

import utime
import urandom

ledOn = False #Boolean to indicate whether the LED is on
buttonPressed = False #Boolean to indicate whether the user button has been pressed
reactionTime = 0 # Int to store value of counter in callback function


##Interrupt callback function: Handles interrupt caused by pressing the user button (B1)
#
#Checks if the global variable ledOn is True, indicating that the player is allowed to press the button.
#Then, it sets the global reaction time variable and turns the LED off.
#@param which_pin Int used to statisfy interrupt callback function format
def buttonPressISR(which_pin):
	global ledOn
	if(ledOn == True):
		global reactionTime
		reactionTime = reactionTimer.counter()
		global buttonPressed
		buttonPressed = True
		global blinkLED
		blinkLED.low()
	
def timerCB(timer):
	pass


if __name__ == "__main__":
	reactionTimer = pyb.Timer(2, prescaler = 80000000, period = 0x3FFFFFFF) #Timer object used to get accurate timing, prescaler set to make 80 MHz clock into 1KHz clock
	reactionTimer.callback(timerCB) #Setting callback function for the reaction timer
	blinkLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP) #Initializing the LED used to blink
	buttonExtInt = pyb.ExtInt(pyb.Pin.board.PC13,	#Initialzing button interupt, button B1 uses PC13
		pyb.ExtInt.IRQ_FALLING,						#Interrupt on falling edge
		pyb.Pin.PULL_UP,							#Set to Pull Up mode
		buttonPressISR)								#Set callback function from above
	while(True):
		if(buttonPressed == True):
			print("\nYour reaction time was: " + str(float(reactionTime/1000)) + " s\n") #divide by 1000 to convert Khz to Hz
			reactionTime = 0
			reactionTimer.counter(0)
			buttonPressed = False
			utime.sleep(2)
		print("The LED will flash in 2-3 seconds...")
		utime.sleep(2 + (.1 * urandom.randrange(0, 9)))
		reactionTimer.counter(0)
		blinkLED.high()
		ledOn = True
		while(buttonPressed == False):
			pass
		blinkLED.low()
		ledOn = False