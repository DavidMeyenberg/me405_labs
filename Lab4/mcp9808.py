'''
@file mcp9808.py

@brief This code checks the connection of the mcp9808 and reads the temperature in 
       Celcius and Fahrenheit. 

@author Ryan McMullen and David Meyenberg

@copyright CPE/MECH TEAM

@date February 9, 2021
'''

from pyb import I2C
import struct

class mcp9808:
    '''
    @brief allows the user to communicate with an MCP9808 temperature sensor
           using its I2C interface. It contains 3 methods, which are shown below.
    '''
    def __init__(self, I2C, Address):    
        '''
        @brief creates an I2C object 
        '''
        ## address of where the i2c object is located on the bus
        self.address = Address
        
        ## pass in the i2c object so it doesn't need to be passed in method
        self.i2c = I2C
        
    def check(self):
		'''
		@brief a check to verify that the sensor is attached at the given bus 
				address by checking that the value in the manufacturer ID 
				register is correct. See data sheet pages (16-17) to double 
				check this information. If the correct bus address is used, the
				module will 1. If the incorrect bus address is used, will return
				a 0.
               
		'''
		## reads 2 bytes from address specified starting at memory location 6 
			# within the I2C device. location 6 is the manufacturer ID register.
			# (see page 16 of the data sheet). It then concatonates this to a 
			# string.
		val = str(self.i2c.mem_read(2, self.address, 6))

		## uses python's built in int function to convert from binary to an
			# integer. 2 is the base of inputed number system (binary). If the
			# number is equal to 84, which is the contents of the manufacturer ID 
			# register, function will return 1.. if not will return 0.
		print(struct.unpack('>d', val))
		print(struct.unpack('<d', val))
		if(val == b'\x00T'):
			print("Correct Manufacturer ID")
		else:
			print("Wrong Manufacturer ID")
		return
        
    def celsius(self):
        '''
        @brief method returns the measured temperature in degrees celsius.
               The following code was found at:
               https://learn.adafruit.com/micropython-hardware-i2c-devices/i2c-main
               and is used to pull out a signed 12-bit value in degrees celsius
               with a simple fuction from the mcp9808.
        '''
        ## with register pointer set to 5, will return temperature date. See 
         # page 16 of data sheet for more information.
        val = self.i2c.mem_read(2,self.address,5)

        ## bitwise left shift to the left and bitwise or operator
        value = val[0] << 8 | val[1]
        
        ## bitwise and divided by 16
        self.temp = (value & 0xFFF) / 16.0
        
        if value & 0x1000:
            
            self.temp -= 256.0
            
        ## return the temperature in degrees C.    
        return self.temp
    
    def fahrenheit(self):
        '''
        @brief method returns the measured temperature in degrees fahrenheit.
               The following code was found at:
               https://learn.adafruit.com/micropython-hardware-i2c-devices/i2c-main
               and is used to pull out a signed 12-bit value in degrees celsius
               with a simple fuction from the mcp9808.
        '''
        ## with register pointer set to 5, will return temperature date. See 
         # page 16 of data sheet for more information.
        val = self.i2c.mem_read(2, self.address, 5)

        ## bitwise left shift to the left and bitwise or operator
        value = val[0] << 8 | val[1]
        
        ## bitwise and divided by 16
        self.temp = (value & 0xFFF) / 16.0
        
        if value & 0x1000:
            
            self.temp -= 256.0
           
         ## convert temperature to farenheit from celsius
        self.temp = self.temp*9/5+32 
           
        ## return the temperature in degrees C.  
        return self.temp