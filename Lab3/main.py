## @file main.py
#	This program records a ADC reading of the user button press and sends the data over a serial connection to an additional program 
#	that plots and saves it.
#
#	The program waits on the serial connection for the user to send a 'G'. Then it waits for the user to press the user button (B1).
#	The ADC is read until it detects a rising value. Then it reads into a buffer, which is then sent over the serial connection.
#	The ADC is connected to the user button with a connection from PC13 to PA0. To create a good reading, a press, short pause, 
#	and then release is recommended.
#
#  @author David Meyenberg
#
#  @copyright License Info
#
#  @date February 4, 2021
#
from pyb import UART
import array

myUART = UART(2)

## Recieves a char over the serial connection
#
# Reads a char and if it is not 'G', it sends a reply message.
def recvChar():
	if(myUART.any() != 0):
		val = myUART.readchar()
		if(val != 71):
			myUART.write('You sent a ' + chr(val) + ' to the Nucleo. Send a G to begin recording.')
		return val

buttonPressed = False #Boolean to indicate whether the user button has been pressed

##Interrupt callback function: Handles interrupt caused by pressing the user button (B1)
#
#Checks if the global variable ledOn is True, indicating that the player is allowed to press the button.
#Then, it sets the global reaction time variable and turns the LED off.
#@param which_pin Int used to statisfy interrupt callback function format
def buttonPressISR(which_pin):
		global buttonPressed
		buttonPressed = True

buttonExtInt = pyb.ExtInt(pyb.Pin.board.PC13,	#Initialzing button interupt, button B1 uses PC13
		pyb.ExtInt.IRQ_FALLING,						#Interrupt on falling edge
		pyb.Pin.PULL_NONE,							#Set to Pull none mode
		buttonPressISR)								#Set callback function from above

timer6 = pyb.Timer(6, freq = 200000)	#Initializing timer with a frequency of 200 KHz 	
buffer = array.array('H', (0 for n in range(1000)))	#Initializing array to hold data values
#A frequency of 200 KHz and a buffer size of 1000 results in a sample time of 2 ms

adc = pyb.ADC(pyb.Pin.board.PA0)	#Initializing ADC on pin analog-in pin PA0 (A1)



if __name__ == "__main__":
	while(recvChar() != 71): #waiting for user to press 'G'
		pass	
	while(True):
		if(buttonPressed == True):
			buttonPressed = False
			while(adc.read() < 10): #removes neglible data
				pass
			adc.read_timed(buffer, timer6)
			for i in range(0, 1000):
				myUART.write(str(buffer[i]) + '\n')