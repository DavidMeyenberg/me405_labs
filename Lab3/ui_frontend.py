## @file ui_frontend.py
#	This program handles the user interface for communicating over the serial connection. It receives ADC readings
#   sent from a Nucleo board and plots it on a graph. It also saves these values in a .csv file.
#
#	The program waits for a user to enter a 'G'. It then receives and saves the batch of values being sent over the
#   serial connection and adjusts the values to represent voltage. 
#
#  @author David Meyenberg
#
#  @copyright License Info
#
#  @date February 4, 2021
#

import serial
import math
from matplotlib import pyplot

#Initalizes serial communication
ser = serial.Serial(port='COM3', baudrate=115000, timeout=1)

# Sends character over serial connection
def sendChar():
    inv = input('Send a character: ')
    ser.write(str(inv).encode('ascii'))
    return inv

#   Receives message over serial connection
def recvMessage():
        return ser.readline().decode('ascii')

#Initializes arrays for x and y axis of the plot
times = [(t*2)/1000 for t in range(1000)]
values = [0 for t in range(1000)]


#Waits for user to press 'G'
print("Send a G to begin recording.");
while(sendChar() != 'G'):
    print(recvMessage())

#Reads in data from Nucleo over the serial connection. Expects a value between 0 and 4096
#representing voltage values from 0 to 3.3 V 
for i in range(1000):
    message = recvMessage()
    if(message != ''):
        values[i] = (int(message) * 3.3)/(4096)

#Plots the time and voltage values using pyplot
pyplot.plot(times, values, "g--")
pyplot.xlabel("Times (ms)")
pyplot.ylabel("Values (V)") 
pyplot.title("Button Press ADC Reading")
pyplot.show()

#Exports the time and voltage values as a comma-seperated value file
csvFile = open("buttonVoltage.csv", 'w')
for i in range(1000):
    csvFile.write(str(times[i]) + ", " + str(values[i]) + "\n")
csvFile.close()

#Closes serial connection
inv = input('Send any character to close connection. ')
ser.write(str(inv).encode('ascii'))
ser.close()