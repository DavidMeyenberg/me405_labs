## @file lab1.py
#  Brief doc for lab1.py
#
#  Detailed doc for lab1.py 
#
#  @author David Meyenberg
#
#  @copyright License Info
#
#  @date January 14, 2021
#
#  @package vendotron
#  Brief doc for the vendotron module
#
#  Detailed doc for the vendotron module
#
#  @author David Meyenberg
#
#  @copyright License Info
#
#  @date January 14, 2021

import keyboard

pushed_key = None

def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key
    pushed_key = thing.name

state = 0
balance = 0
COIN = 0
EJECT = 1
DRINK = 2
INVALID = 3
EMPTY = 4
coin_amount = (1, 5, 10, 25, 100, 500, 1000, 2000)


##Prints welcome message
#
#Detailed info on print welcome function
def printWelcome():
    print('Welcome to Vendotron! Please enter a coin or dollar (0-7), select a drink (c, p, s, d), or eject the remaining balance(e):')

##Gets change in denominations
#
#Detailed info on get change function
#@param price Integer representing the price of an item in cents
#@param payment Tuple representing number of pennies, nickels, dimes, quaters, ones, fives, tens, twenties
def getChange(price, payment):
    changeList = [0, 0, 0, 0, 0, 0, 0, 0]
    change_amount = payment - price
    i = 7
    while(i >= 0):
        if(change_amount >= coin_amount[i]):
            change_amount -= coin_amount[i]
            changeList[i] += 1
        else:
            i -= 1
    change = (changeList[0], changeList[1], changeList[2], changeList[3], changeList[4], changeList[5], changeList[6], changeList[7])
    return change

##Interprets user input
#
#Detailed info on take user input function
#@param balance Integer representing the remaining balance in cents
def takeUserInput(balance):
    selected_drink_amount = 0
    global pushed_key
    user_input = pushed_key
    pushed_key = None
    if(user_input == None):
        return (EMPTY, balance, selected_drink_amount)
    elif(user_input == '0' or user_input == '1' or user_input == '2' or user_input == '3' or user_input == '4' or user_input == '5' or user_input == '6' or user_input == '7'):
        balance += coin_amount[int(user_input)]
        return (COIN, balance, selected_drink_amount)
    elif(user_input == 'e'):
        return (EJECT, balance, selected_drink_amount)
    elif(user_input == 'c'):
        selected_drink_amount = 100
        return (DRINK, balance, selected_drink_amount)
    elif(user_input == 'p'):
        selected_drink_amount = 120
        return (DRINK, balance, selected_drink_amount)
    elif(user_input == 's'):
        selected_drink_amount = 85
        return (DRINK, balance, selected_drink_amount)
    elif(user_input == 'd'):
        selected_drink_amount = 110
        return (DRINK, balance, selected_drink_amount)
    else:
        return (INVALID, balance, selected_drink_amount)
    
##Prints remaining balance
#
#Detailed info on display balance function
def displayBalance():
    print(balance)

if __name__ == "__main__":

    keyboard.on_press (on_keypress)

    while True:
        if state == 0:
            printWelcome()
            state = 1
        elif state == 1:
            (user_input, balance, selected_drink_amount) = takeUserInput(balance)
            if(user_input == COIN):
                displayBalance()
                state = 1
            elif(user_input == EJECT):
                return_denoms = getChange(0, balance)
                print('The following denominations were returned: ')
                print(return_denoms)
                state = 1    
            elif(user_input == DRINK):
                state = 2
            elif(user_input == EMPTY):
                state = 1
            elif(user_input == INVALID):
                print('Invalid input')
                state = 1
        elif state == 2:
            if(balance > selected_drink_amount):
                return_denoms = getChange(selected_drink_amount, balance)
                balance = 0
                print('Drink sent')
                print('The following denominations were returned: ')
                print(return_denoms)
                #drink is sent, remaining balance return
            else:
                print('Insufficient funds')
                print('Remaining balance: ' + str(balance))
                #insufficent funds, display price of selected item.
            state = 1












