'''
@file EncoderDriver.py

@brief This code creates a EncoderDriver class to measure the movement of the encoder angle with an optical encoder.

@author David Meyenberg and Ryan McMullen

@date March 11, 2021

'''
import pyb

class EncoderDriver:
	'''
	@brief Allows the user to read angle measurements from the encoder.
	'''
	def __init__(self, encoderA_pin, encoderB_pin, timerNumber, prescaler, period):
		'''
		@brief Creates a EncoderDriver object to read an encoder.
		@param encoderA_pin A pyb.Pin object used as the encoder's A pin.
		@param encoderB_pin A pyb.Pin object used as the encoder's B pin.
		@param timerNumber A value used to create a pyb.Timer object to count for the encoder.
		@param prescaler A value used for the timer.
		@param period A value used for the timer.
		'''
		self.EncoderA_pin = encoderA_pin
		self.EncoderB_pin = encoderB_pin
		self.TimerNumber = timerNumber
		self.Prescaler = prescaler
		self.Period = period
		
		#Creates a pyb.Timer object using the timing number, prescaler, and period provided
		self.Timer = pyb.Timer(self.TimerNumber, prescaler = self.Prescaler, period = self.Period)
		self.Timer.channel(1, self.Timer.ENC_A, pin = self.EncoderA_pin)
		self.Timer.channel(2, self.Timer.ENC_B, pin = self.EncoderB_pin)
		
		#Stores timer counts
		self.CurCount = 0
 		self.PrevCount = 0
		
		#Stores position and change in position
		self.Position = 0
		self.Delta = 0
		

	def update(self):
		'''
		Updates the position of the encoder and calculates the amount of change in position to store in delta.
		If theres is overflow or underflow, the delta is adjusted.
		The delta value is added to the current position.
		'''
		self.CurCount = self.Timer.counter()

		self.Delta = self.CurCount - self.PrevCount
		#Overflow: Reduces delta by a period
		if(self.Delta > (self.Period/2)):
			adjustedDelta = self.Delta - self.Period;
			self.Position += adjustedDelta;
		#Underflow: Increases delta by a period
		elif(self.Delta < (-self.Period/2)):
			adjustedDelta = self.Delta + self.Period;
			self.Position += adjustedDelta
		#Normal case: No delta adjustment
		else:
			self.Position += self.Delta
		self.PrevCount = self.CurCount

	def get_position(self):
		'''
		Returns the most recently updated position of the encoder.
		'''
		return self.Position

	def set_position(self, position):
		'''
		Sets the position of the encoder.
		'''
		self.Position = position
		self.CurCount = position
		self.PrevCount = position

	def get_delta(self):
		'''
		Returns the amount of change in position since the last update.
		'''
		return self.Delta

	def zero():
		'''
		Sets the position of the encoder to 0.
		'''
		self.set_position(0)

if __name__ =='__main__':
	#encoder 1 pins
	Encoder1APin = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
	Encoder1BPin = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
	#encoder 1 timer number
	TimerNum1 = 4

	#encoder 2 pins
	Encoder2APin = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
	Encoder2BPin = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
	#encoder 2 timer number
	TimerNum2 = 8

	Period = 65535
	Prescaler = 0
	Encoder1 = EncoderDriver(Encoder1APin, Encoder1BPin, TimerNum1, Prescaler, Period)
	Encoder2 = EncoderDriver(Encoder2APin, Encoder2BPin, TimerNum2, Prescaler, Period)
	while(True):
		Encoder1.update()
		print(Encoder1.get_position())