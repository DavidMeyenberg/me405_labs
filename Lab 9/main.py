'''
@file main.py

@brief 

@author David Meyenberg

@date March 17, 2021
'''

import pyb
import utime
import math
from EncoderDriver import EncoderDriver
from MotorDriver import MotorDriver
from TouchPanel import TouchPanelDriver


if __name__ == "__main__":
	#TOUCH PANEL
	#width of 105 and length of 182 measured as distances between maximum and minimum readings
	TPD = TouchPanelDriver(pyb.Pin.board.PA7, pyb.Pin.board.PA1, pyb.Pin.board.PA6, pyb.Pin.board.PA0, 105, 182)
	x = 0

	#ENCODERS
	#encoder 1 pins
	Encoder1APin = pyb.Pin(pyb.Pin.cpu.B6, pyb.Pin.IN)
	Encoder1BPin = pyb.Pin(pyb.Pin.cpu.B7, pyb.Pin.IN)
	#encoder 2 pins
	Encoder2APin = pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN)
	Encoder2BPin = pyb.Pin(pyb.Pin.cpu.C7, pyb.Pin.IN)
	#encoder 1 timer number
	TimerNum1 = 4
	#encoder 2 timer number
	TimerNum2 = 8
	Period = 65535
	Prescaler = 0
	Encoder1 = EncoderDriver(Encoder1APin, Encoder1BPin, TimerNum1, Prescaler, Period)
	Encoder2 = EncoderDriver(Encoder2APin, Encoder2BPin, TimerNum2, Prescaler, Period)
	theta_y = 0

	#MOTORS
    # CPU pin which is connected to DRV8847 nSLEEP pin 
	pin_nSLEEP = pyb.Pin.board.PA15;
	# CPU pin which is connected to DRV8847 IN1 pin
	pin_IN1    = pyb.Pin.board.PB4;
    # CPU pin which is connected to DRV8847 IN2 pin
	pin_IN2    = pyb.Pin.board.PB5;
	# CPU pin which is connected to DRV8847 IN3 pin
	pin_IN3    = pyb.Pin.board.PB0;
	# CPU pin which is connected to DRV8847 IN4 pin
	pin_IN4    = pyb.Pin.board.PB1;

	# Create the timer object used for PWM generation. Numbers were taken from   
	time = pyb.Timer(3,freq = 20000);
	
	# Create a motor object passing in the pins and timer
	#Motor1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, time, 1, 2)
	Motor2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, time, 3, 4)
    
	# Enable the motor driver
	#Motor1.enable()
	Motor2.enable()

	period = 100	#TODO assign period
	K1 = -3.2839
	K2 = -0.2771
	K3 = -9.0905
	K4 = -3.9995
	#K_duty = R/(Vdc * Kt) = 2.21 ohms /(12V * 12.8 mN/A)
	#K_duty = 13.3454	
	K_duty = 20

	old_time = 0

	while(True):
		new_time = utime.ticks_us() / 1000000
		period = new_time - old_time
		old_time = new_time

		x_new = TPD.ScanX() / 1000		
		#x_new = -x_new
		x_dot = (x_new - x) / period			
		x = x_new
	
		#TODO check for correct encoder
		Encoder2.update()
		theta_y_new = Encoder2.get_position() * (math.pi / 1000)
		theta_dot_y = (theta_y_new - theta_y) / period
		theta_y = theta_y_new

		Torque_A = (-K1 * x_dot) - (K2 * theta_dot_y) - (K3 * x) - (K4 * theta_y)
		Duty_Cycle_A = int(K_duty * Torque_A)
		print(Duty_Cycle_A)
		Motor2.set_duty(Duty_Cycle_A)
		#print("x: " + str(x))
		#print("x_dot: " + str(x_dot))
		#print("theta_y: " + str(theta_y))
		#print("theta_dot_y: " + str(theta_dot_y))
		#print("Encoder position: " + str(Encoder2.get_position()))
		
